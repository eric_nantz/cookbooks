#
# Cookbook Name:: phantomjs
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

build_options = node['phantomjs']['build_options'].join(' ')
prefix_dir = node['phantomjs']['prefix_dir']

include_recipe 'build-essential'
include_recipe 'yum-epel' if node['platform_family'] == 'rhel'
include_recipe 'apt' if node['platform_family'] == 'debian'

node['phantomjs']['src_deps'].each do |pkg|
  package pkg do
    action :install
  end
end

# create swap file
swap_file '/swapfile' do
  size 2048
end

# clone phantomjs git repo
execute "clone phantomjs git repo" do
  cwd Chef::Config['file_cache_path']
  command <<-COMMAND
  git clone git://github.com/ariya/phantomjs.git 
  COMMAND
  not_if "test -f phantomjs/build.sh"
 end

# build phantomjs from source
bash 'build phantomjs' do
  cwd Chef::Config['file_cache_path']
  code <<-EOF
  (cd phantomjs && ./build.sh #{build_options})
  EOF
  not_if "test -f #{Chef::Config['file_cache_path']}/phantomjs/bin/phantomjs"
end

# copy binary to /usr/local/bin
bash 'copy to bin' do
  cwd Chef::Config['file_cache_path']
  code <<-EOF
  cp #{Chef::Config['file_cache_path']}/phantomjs/bin/phantomjs #{prefix_dir}
  EOF
  not_if "test -f #{prefix_dir}/phantomjs"
end

## build phantom js
#execute "install phantomjs" do
#  cwd Chef::Config['file_cache_path']
#  command <<-COMMAND
#  cd phantomjs && \
#  ./build.sh --confirm --jobs 2
#  COMMAND
#  not_if "test -f #{Chef::Config['file_cache_path']}/phantomjs/bin/phantomjs"
#end


