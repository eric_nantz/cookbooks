default['libicu-devel']['version'] = '4.2.1-9.1.el6_2'
default['libicu-devel']['url'] = "http://mirror.centos.org/centos/6/os/x86_64/Packages/libicu-devel-#{node['libicu-devel']['version']}.x86_64.rpm"
default['gperf']['version'] = '3.0.3-9.1.el6'

default['gperf']['url'] = "http://mirror.centos.org/centos/6/os/x86_64/Packages/gperf-#{node['gperf']['version']}.x86_64.rpm"
default['phantomjs']['version'] = '2.0'
default['phantomjs']['prefix_dir'] = '/usr/local/bin'
default['phantomjs']['bin'] = 'phantomjs'

case node["platform_family"]
when "debian"
  default['phantomjs']['src_deps'] = %w{ build-essential g++ flex bison gperf ruby perl libsqlite3-dev libfontconfig1-dev libicu-dev libfreetype6 libssl-dev libpng-dev libjpeg-dev python libx11-dev libxext-dev git-core }
when "rhel"
  default['phantomjs']['src_deps'] = %w{ gcc-c++ make git flex bison ruby openssl-devel freetype-devel fontconfig-devel sqlite-devel libpng-devel libicu libicu-devel gstreamer-plugins-base-devel libjpeg-devel gperf }
end

default['phantomjs']['jobs'] = node['cpu']['total']
default['phantomjs']['build_options'] = %W(--confirm
                                           --jobs #{phantomjs['jobs']})


