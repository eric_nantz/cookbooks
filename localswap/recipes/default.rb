#
# Cookbook Name:: localswap
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

swap_file '/swapfile' do
  size 1024
  action :create
  persist true
end

