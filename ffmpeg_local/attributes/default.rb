default['ffmpeg']['version'] = '2.6.3'
default['ffmpeg']['prefix'] = '/usr/local'
default['ffmpeg']['url'] = "http://ffmpeg.org/releases/ffmpeg-#{node['ffmpeg']['version']}.tar.bz2"
default['ffmpeg']['config_opts'] = "--enable-shared --enable-nonfree --enable-gpl --enable-decoder=aac --enable-demuxer=mov --enable-x11grab --enable-zlib --enable-protocol=http --enable-filter=aformat --enable-filter=volume --enable-filter=aresample"
