#
# Cookbook Name:: localusers
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
# 
# recipe for adding my personal user to a server

user 'random' do
  supports :manage_home => true
  comment 'Random User'
  uid 1234
  gid 'users'
  home '/home/random'
  shell '/bin/bash'
  password '$1$vVk9TvFO$NRaoDefkEQ8lhzri/6Vff0'
end
