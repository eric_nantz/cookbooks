#
# Cookbook Name:: localusers
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

search(:users, "*:*").each do |data|
	user data["id"] do
	    supports :manage_home => true
		comment data["comment"]
		uid data["uid"]
		gid data["gid"]
	    shell data["shell"]
		home data["home"]
		password data["password"]
		ssh_keys data["ssh_keys"]
   end	
end

user 'random' do
  supports :manage_home => true
  comment 'Random User'
  uid 1234
  gid 'users'
  home '/home/random'
  shell '/bin/bash'
  password '$1$vVk9TvFO$NRaoDefkEQ8lhzri/6Vff0'
end
